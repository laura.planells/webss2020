# Generated by Django 3.0.4 on 2020-04-22 15:18

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('spiel', '0001_initial'),
    ]

    operations = [
        migrations.RenameModel(
            old_name='Benutzername',
            new_name='User',
        ),
    ]
