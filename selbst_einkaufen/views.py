from django.shortcuts import render, redirect
from django.http import HttpResponse
from . import models

def index(request):
    context = {}
    context['einkaufen'] = models.Item.objects.filter(erledigt=False)
    context['erledigt'] = models.Item.objects.filter(erledigt=True)

    return render(request, 'selbst_einkaufen/index.html', context)
# Create your views here.
def add(request):
    if 'add' in request.POST:
        item = models.Item()
        item.text = request.POST ['produkt']
        item.menge = request.POST ['menge']
        item.einheit = request.POST ['einheit']
        item.save()
    return redirect('index')

def refresh(request):
    if 'refresh' in request.POST:
        einkaufen = models.Item.objects.filter(erledigt=False)
        erledigt = models.Item.objects.filter(erledigt=True)
        gerade_erledigt = []
        for item in einkaufen:
            if str(item.id) in request.POST.getlist('neu_erledigt'):
                item.erledigt = True
                gerade_erledigt.append (str(item.id))
                item.save()
        for item in erledigt:
            if str(item.id) not in request.POST.getlist('bleiben_erledigt') and str(item.id) not in gerade_erledigt:
                item.erledigt = False
                item.save()
        return redirect('index')
