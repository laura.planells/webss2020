from django.db import models
from django.db.models import TextField


class Item(models.Model):
    text = models.TextField (blank=False, null=False)
    menge = models.FloatField (blank=True, default="")
    einheit = models.TextField (blank=True, default="")
    erledigt = models.BooleanField(default=False)

    def __str__(self):
        erledigt_string = " "
        if self.erledigt:
            erledigt_string = "X"
        return "[{}] {} ({} {}) ".format(erledigt_string, self.text, self.menge, self.einheit)



# Create your models here.
